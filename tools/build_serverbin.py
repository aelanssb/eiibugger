data = None

with open("bin/server.bin", "rb") as f:
    data = f.read()

with open("bin/server_bin.h", "w") as f:
    header = "#pragma once\nstatic const unsigned char server_bin[] = {\n"

    if type(data[0]) is int:
        for byte in data:
            header += ' 0x%02x,' % byte
    else:
        for byte in data:
            header += ' 0x%02x,' % ord(byte)

    header = header.rstrip(",\n ")
    header += "\n};\nstatic const unsigned int server_length = "
    header += str(len(data)) + ";\n"

    f.write(header)
