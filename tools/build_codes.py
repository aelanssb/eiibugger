import re
from os import path, linesep
from subprocess import Popen, check_output, PIPE
from sys import stdout
from glob import glob

functions = {
    'sss_getCursorType': 0x0C98C738,
    'isButtonDown': 0x0D09884C
}

source_files = [path.splitext(path.basename(p))[0] for p in glob('codes/*.s')]
source_files.remove('common')

# assemble
for name in source_files:
    with open('codes/'+name+'.s', 'r') as file:
        file_data = file.read()
        file_data = '.include "codes/common.s"\n' + file_data + '\n'

        p = Popen(['powerpc-eabi-as', '-o', 'bin/codes/'+name+'.o', '--'], stdin=PIPE)
        p.communicate(file_data)

object_files = ['bin/codes/'+name+'.o' for name in source_files]

# collect section addresses
sections = []
for obj_file in object_files:
    data = check_output(['powerpc-eabi-objdump', '-D', obj_file])
    lines = data.split('\r\n')
    for line in lines:
        m = re.match('Disassembly of section \.(.*):', line)
        if m != None:
            sections.append(m.group(1))

# build linker script
with open('bin/codes/link.ld', 'w') as file:
    for func_name in functions:
        file.write('%s = 0x%08X;\n' % (func_name, functions[func_name]))

    file.write('\nSECTIONS {')
    for section in sections:
        file.write('\n\t. = %s;\n' % section)
        file.write('\t.%s : {}\n' % section)
    file.write('}\n')

# link objects
check_output(['powerpc-eabi-ld', '-T', 'bin/codes/link.ld', '-o', 'bin/codes/out.elf'] + object_files)

# build final output
data = check_output(['powerpc-eabi-objdump', '-D', 'bin/codes/out.elf'])

codes = []
currentCode = None

lines = data.split(linesep)
for line in lines:
    # start of code
    m = re.match('Disassembly of section \.(.*):', line)
    if m != None:
        # output += '\n@' + m.group(1)[2:] + '\n'
        if currentCode != None:
            codes.append(currentCode)

        currentCode = {
            'address': m.group(1)[2:],
            'instructions': []
        }
        continue

    # instruction
    m = re.match(' [0-9a-f]{7}:\t(.. .. .. .. )\t(.*)', line)
    if m != None:
        # output += '  ' + m.group(1).replace(' ', '') + ' # ' + m.group(2) + '\n'
        currentCode['instructions'].append(m.group(1).replace(' ', ''))
        continue

#
output = ''
output += '#pragma once\n\n'
output += 'u32 codeData[] = { '
output += str(len(codes))

for code in codes:
    output += ', 0x%s' % code['address']
    output += ', %d' % len(code['instructions'])
    for inst in code['instructions']:
        output += ', 0x%s' % inst

output += '};'

with open('bin/codes.h', 'w') as file:
    file.write(output)
