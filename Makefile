CFLAGS=-O2 -Wall -ffreestanding -mrvl -mcpu=750 -meabi -mhard-float -fshort-wchar -msdata=none -memb -ffunction-sections -fdata-sections -Wno-strict-aliasing -Wno-write-strings
LDFLAGS=
CXX=powerpc-eabi-gcc
LD=powerpc-eabi-ld
AS=powerpc-eabi-as

all:
	mkdir -p bin/codes

	python tools/build_codes.py

	$(CXX) $(CFLAGS) -Ibin -c -o bin/server.o src/server.cpp
	$(LD) $(LDFLAGS) -o bin/server.bin -T src/server.ld bin/server.o
	python tools/build_serverbin.py

	$(CXX) $(CFLAGS) -Ibin -c -o bin/installer.o src/installer.cpp
	$(LD) $(LDFLAGS) -o bin/code.bin bin/installer.o -T src/installer.ld

	cp bin/code.bin C:/xampp/htdocs/payload/diibugger.bin
