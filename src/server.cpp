#include "common.h"
#include "server.h"
#include "codes.h"

int _start (int argc, char *argv[])
{
    u64 titleId = *((u64*)0x10013C10);

    if (titleId == 0x0005000010144F00) {
        u32 pos = 0;
        u32 numCodes = codeData[pos++];

        for (u32 codeIdx = 0; codeIdx < numCodes; ++codeIdx) {
            u32 *ptr = (u32*)(0xA0000000 + codeData[pos++]);
            u32 numInstructions = codeData[pos++];

            memcpy(ptr, codeData + pos, numInstructions * 4);
            DCFlushRange(ptr, numInstructions * 4);
            ICInvalidateRange(ptr, numInstructions * 4);

            pos += numInstructions;
        }
    }

    return main(argc, argv);
}
