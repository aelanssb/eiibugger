#pragma once

extern "C"
{
    extern int (* const entry_point)(int argc, char *argv[]);
#   define main (*entry_point)

    extern void* memcpy(void *dst, const void *src, int bytes);
    extern void* memset(void *dst, int val, int bytes);

    extern void DCFlushRange(const void *addr, int length);
    extern void ICInvalidateRange(const void *addr, int length);

    extern int OSDynLoad_Acquire(const char *rpl, int *handle);
    extern int OSDynLoad_FindExport(unsigned int handle, int isdata, const char *symbol, void *address);
}

typedef struct FSClient
{
    u8 buffer[5888];
} FSClient;

typedef struct FSCmdBlock
{
    u8 buffer[2688];
} FSCmdBlock;

typedef struct FSStat
{
    u32 flags;
    u8 _4[0xC];
    u32 size;
    u8 _14[0x50];
} FSStat;

typedef struct FSDirEntry
{
    FSStat stat;
    char name[256];
} FSDirEntry;
